package main

import (
	"encoding/json"
	"log"
	"net/http"
	"os"

	"gitlab.com/VagueCoder0to.n/GORM-Postgres-Example/app/database"
)

func main() {
	logger := log.New(os.Stdout, "[Database-Controller] ", log.Lshortfile|log.LstdFlags)
	logger.Println("Database Controller Initiated")

	db, err := database.NewDBClient(logger)
	if err != nil {
		logger.Fatal(err)
	}
	logger.Printf("Connection to Postgres Server Successful!")

	db.Client.AutoMigrate(database.Artist{})
	db.Client.AutoMigrate(database.Movie{})
	db.Client.AutoMigrate(database.User{})
	db.Client.AutoMigrate(database.Reviews{})

	sample := db.Client.Find(database.Artist{})
	logger.Println(sample)

	http.HandleFunc("/", func(rw http.ResponseWriter, r *http.Request) {
		err := json.NewEncoder(rw).Encode(&sample)
		if err != nil {
			logger.Println(err)
		}
	})

	logger.Println("Serving on Port 8080")
	http.ListenAndServe(":8080", nil)
}

// $ psql -h localhost -p 5432 -U postgres
// Password for user postgres:
// psql (12.6 (Ubuntu 12.6-0ubuntu0.20.04.1))
// SSL connection (protocol: TLSv1.3, cipher: TLS_AES_256_GCM_SHA384, bits: 256, compression: off)
// Type "help" for help.

// postgres=# \d
//                List of relations
//  Schema |      Name      |   Type   |  Owner
// --------+----------------+----------+----------
//  public | artists        | table    | postgres
//  public | artists_id_seq | sequence | postgres
//  public | movies         | table    | postgres
//  public | movies_id_seq  | sequence | postgres
//  public | reviews        | table    | postgres
//  public | reviews_id_seq | sequence | postgres
//  public | users          | table    | postgres
//  public | users_id_seq   | sequence | postgres
// (8 rows)
//                       ^
// postgres=# SELECT * FROM artists;
//  id | created_at | updated_at | deleted_at | name | role
// ----+------------+------------+------------+------+------
// (0 rows)

// postgres=# SELECT * FROM movies;
//  id | created_at | updated_at | deleted_at | title | release_date
// ----+------------+------------+------------+-------+--------------
// (0 rows)

// postgres=# SELECT * FROM users;
//  id | created_at | updated_at | deleted_at | name | contact
// ----+------------+------------+------------+------+---------
// (0 rows)

// postgres=# SELECT * FROM reviews;
//  id | created_at | updated_at | deleted_at | comment
// ----+------------+------------+------------+---------
// (0 rows)

// postgres=# \q
