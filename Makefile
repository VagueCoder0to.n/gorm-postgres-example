#!make
include ./app/config/dev.env
export

GIT_BRANCH := $(shell git rev-parse --abbrev-ref HEAD)

vars:
	echo ${POSTGRES_PASSWORD}
	echo ${PSQL_HOST}
	echo ${PSQL_PORT}

postgres:
ifeq ($(shell docker ps -a | awk '{if($$NF=="postgres_service_standalone"){print $$NF}}'), )
	- docker container run -d --rm -p ${PSQL_PORT}:5432 -e POSTGRES_PASSWORD=${POSTGRES_PASSWORD} -e PGDATA=${PGDATA} -v /postgres/pgdata:${PGDATA} --name postgres_service_standalone postgres:13.2-alpine
endif

kill_postgres:
	- docker ps -a | awk '{if($$NF=="postgres_service_standalone"){print $$1}}' | xargs -r docker rm -f

run_app:
	- go run $(PWD)/app/*.go

execute_app:
	- go build -o Database-Controller $(PWD)/app
	- ./Database-Controller

tests:
	- export PSQL_HOST=localhost && export PSQL_PORT=5433 && go test -v ./...

tests_in_gitlab_ci:
	- export PSQL_HOST=postgres && export PSQL_PORT=5433 && go test -v ./...

build:
	docker build -t gorm-postgres-example/database-controller:1.0.0 ./app

run:
ifeq ($(shell docker network ls | awk '{if($$2=="gorm-postgres-net"){print $$2}}' | xargs -r), )
	# - docker network create gorm-postgres-net
	include ./app/config/prd.env DESTDIR=.	
	echo ${PSQL_PORT}
endif
# ifeq ($(shell docker ps -a | awk '{if($$NF=="postgres_service"){print $$NF}}'), )
# 	- docker container run -d --rm -p ${PSQL_PORT}:5432 --network gorm-postgres-net -e POSTGRES_PASSWORD=${POSTGRES_PASSWORD} -e PGDATA=${PGDATA} -v /postgres/pgdata:${PGDATA} --name postgres_service postgres:13.2-alpine
# 	- sleep 10
# endif
	# - docker container run -d --rm -p 8080:8080 --network gorm-postgres-net -e PSQL_HOST=postgres_service -e PSQL_PORT=${PSQL_PORT} --name database-controller gorm-postgres-example/database-controller:1.0.0

kill:
	- docker ps -a | awk '{if($$NF=="postgres_service" || $$NF=="database-controller"){print $$1}}' | xargs -r docker rm -f
	- docker network ls | awk '{if($$2=="gorm-postgres-net") {print $$1}}' | xargs -r docker network rm
	- docker images -a | grep "^<none>" | awk '{print $$3}' | xargs -r docker rmi
	- docker volume ls | awk '{if($$2=="postgres"){print $$2}}' | xargs -r docker volume rm -f
	- sudo rm -rf /postgres/pgdata

call:
	- curl localhost:8080

gitlab_push:
	- git add *
	- git add .gitlab-ci.yml
	- git add .gitignore
ifdef c
	- @echo ${c}
	- git commit -m "${c}"
else
	- git commit -m "Config Correction"
endif
	- git push -u origin ${GIT_BRANCH}


git_merge:
ifeq (,$(and $(filter Changes not staged for commit, $(shell git status)), $(filter Changes to be committed, $(shell git status))))
	- @echo "Changes ommitted/up-to-date for current working branch. Proceeding...";
ifdef to
ifeq (,$(filter $(to), $(GIT_BRANCH)))
ifneq (,$(filter $(to), $(shell git branch)))
	- @echo "Branch '${to}' found. Proceeding...";
	- $(eval CURRENT_BRANCH := $(GIT_BRANCH))
	- @git checkout ${to};
	- @git merge $(CURRENT_BRANCH);
	- @git checkout $(CURRENT_BRANCH);
	- @echo "All changes of '$(CURRENT_BRANCH)' merged with '$(to)'. Back to '$(CURRENT_BRANCH)'.";
else
	- @echo "Exited. Branch '${to}' not found.";
	- @exit 0;
endif
else
	- @echo "Exited. Current branch and merge-to branch cannot be same.";
	- @exit 0;
endif
else
	- @echo "Exited. Provide merge-to branch as to=<branch_name> and retry.";
	- @exit 0;
endif
else
	- @echo "Exited. Please do the add/rm/commit in current branch and retry.";
	- @exit 0;
endif
